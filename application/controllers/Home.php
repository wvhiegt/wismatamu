<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('templates/front');
	}

	public function signin()
	{
		$data['username'] = "";
		if ($this->input->post()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if (strtoupper($username) == 'PENGELOLA') {
				redirect('panel/pengelola');
			} else if (strtoupper($username) == 'RESEPSIONIS') {
				redirect('panel/resepsionis');
			} else if (strtoupper($username) == 'PETUGAS') {
				redirect('panel/petugas');
			} else {
				$data['username'] = $username;
				$this->session->set_flashdata('information', 'Wrong username or password !');
			}
		}

		$data['view'] = 'panel/login';
		$this->load->view('templates/admin', $data); 
	}

	public function booking()
	{
		foreach($this->input->post() as $key => $val) {
			${$key} = $val;
		}
		if ($check_in == '') {
			echo '<div class="error_message">Silahkan tentukan tanggal menginap anda</div>';
		} else if ($wisma == '') {
			echo '<div class="error_message">Silahkan pilih wisma tempat menginap</div>';
		} else if ($wisma == 'Jasmine') {
			echo '<div class="error_message">Mohon maaf, kamar/wisma tidak tersedia</div>';
		} else if ($room_type == '') {
			echo '<div class="error_message">Silahkan tentukan jenis kamar anda</div>';
		} else if ($email == '') {
			echo '<div class="error_message">Silahkan mengisi email anda</div>';
		} else if ($quantity == '') {
			echo '<div class="error_message">Silahkan tentukan jumlah orang yang menginap</div>';
		} else {
			echo '<div id="success_page" style="color:#fff; padding:10px"><strong>Email telah dikirim. </strong>Terima kasih <strong></strong>, Kami akan segera menghubungi anda untuk konfirmasi.</div>';
		}
	}
}
