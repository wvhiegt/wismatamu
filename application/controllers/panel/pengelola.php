<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengelola extends CI_Controller 
{
	private $userdata;
	private $menus;
	function __construct()
	{
		parent::__construct();
		$this->userdata = array(
			'name' => 'Muhamad Luthfie La Roeha',
			'title' => 'Pengelola'
		);

		$this->menus = array(
			array(
				'icon' => 'home',
				'name' => 'Dashboard',
				'link' => 'panel/pengelola',
				'class' => ''
			),
			array(
				'icon' => 'assessment',
				'name' => 'Rekapitulasi Keuangan',
				'link' => 'panel/pengelola/rekap_keuangan',
				'class' => ''
			),
			array(
				'icon' => 'business',
				'name' => 'Rekapitulasi Fasilitas',
				'link' => 'panel/pengelola/rekap_fasilitas',
				'class' => ''
			),
			array(
				'icon' => 'tag-faces',
				'name' => 'Laporan Kepuasan',
				'link' => 'panel/pengelola/laporan_kepuasan',
				'class' => ''
			),
			array(
				'icon' => 'store-mall-directory',
				'name' => 'Kelola Wisma',
				'link' => 'panel/pengelola/kelola_wisma',
				'class' => ''
			),
		);
	}

	public function index()
	{
		$this->menus[0]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/pengelola/dashboard';
        $data['js_plugin'] = array('core/demo/DemoDashboard.js');

		$this->load->view('templates/admin', $data);
	}

	public function rekap_keuangan()
	{
		$this->menus[1]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/pengelola/rekap_keuangan';

		$this->load->view('templates/admin', $data);
	}

	public function rekap_fasilitas()
	{
		$this->menus[2]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/pengelola/rekap_fasilitas';

		$this->load->view('templates/admin', $data);
	}

	public function laporan_kepuasan()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/pengelola/laporan_kepuasan';

		$this->load->view('templates/admin', $data);
	}

	public function kelola_wisma()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/kelola_wisma';

		$this->load->view('templates/admin', $data);
	}

	public function tambah_kamar()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/tambah_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function ubah_detail_kamar()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/ubah_detail_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function tambah_jenis_kamar()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/tambah_jenis_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function update_jenis_kamar()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/update_jenis_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function tambah_fasilitas()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/tambah_fasilitas';

		$this->load->view('templates/admin', $data);
	}

	public function update_fasilitas()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/pengelola/update_fasilitas';

		$this->load->view('templates/admin', $data);
	}
}
