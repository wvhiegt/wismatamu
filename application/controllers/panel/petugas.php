<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller 
{
	private $userdata;
	private $menus;
	function __construct()
	{
		parent::__construct();
		$this->userdata = array(
			'name' => 'Irsyad Iswanda P',
			'title' => 'Petugas'
		);

		$this->menus = array(
			array(
				'icon' => 'find-in-page',
				'name' => 'Periksa Inventaris Kamar',
				'link' => 'panel/petugas/list_kamar',
				'class' => ''
			),
		);
	}

	public function index()
	{
		redirect('panel/petugas/list_kamar');
	}

	public function list_kamar()
	{
		$this->menus[0]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/petugas/list_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function detail_inventaris_kamar()
	{
		$this->menus[0]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/petugas/detail_inventaris_kamar';

		$this->load->view('templates/admin', $data);
	}

	public function update_inventaris_kamar()
	{
		$this->menus[0]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/petugas/update_inventaris_kamar';

		$this->load->view('templates/admin', $data);
	}
}
