<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resepsionis extends CI_Controller 
{
	private $userdata;
	private $menus;
	function __construct()
	{
		parent::__construct();
		$this->userdata = array(
			'name' => 'Achmad Rizky Haqiqi',
			'title' => 'Resepsionis'
		);

		$this->menus = array(
			array(
				'icon' => 'home',
				'name' => 'Dashboard',
				'link' => 'panel/resepsionis',
				'class' => ''
			),
			array(
				'icon' => 'description',
				'name' => 'Reservasi',
				'link' => 'panel/resepsionis/list_kamar_tersedia',
				'class' => ''
			),
			array(
				'icon' => 'hotel',
				'name' => 'Check In',
				'link' => 'panel/resepsionis/list_reservasi',
				'class' => ''
			),
			array(
				'icon' => 'list',
				'name' => 'List Check In',
				'link' => 'panel/resepsionis/list_check_in',
				'class' => ''
			),
			array(
				'icon' => 'account-child',
				'name' => 'Kelola Keluhan',
				'link' => 'panel/resepsionis/kelola_keluhan',
				'class' => ''
			),
		);
	}

	public function index()
	{
		$this->menus[0]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/dashboard';

		$this->load->view('templates/admin', $data);
	}

	public function list_kamar_tersedia()
	{
		$this->menus[1]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/list_kamar_tersedia';

		$this->load->view('templates/admin', $data);
	}

	public function reservasi()
	{
		$this->menus[1]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/reservasi';

		$this->load->view('templates/admin', $data);
	}

	public function list_reservasi()
	{
		$this->menus[2]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/list_reservasi';

		$this->load->view('templates/admin', $data);
	}

	public function check_in()
	{
		$this->menus[2]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/check_in';

		$this->load->view('templates/admin', $data);
	}

	public function list_check_in()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/list_check_in';

		$this->load->view('templates/admin', $data);
	}

	public function ubah_detail_reservasi()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/ubah_detail_reservasi';

		$this->load->view('templates/admin', $data);
	}

	public function check_out()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/check_out';

		$this->load->view('templates/admin', $data);
	}

	public function laporan_check_out()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/laporan_check_out';

		$this->load->view('templates/admin', $data);
	}

	public function form_kepuasan_pelanggan()
	{
		$this->menus[3]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/resepsionis/form_kepuasan_pelanggan';

		$this->load->view('templates/admin', $data);
	}

	public function kelola_keluhan()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/resepsionis/kelola_keluhan';

		$this->load->view('templates/admin', $data);
	}

	public function tambah_keluhan()
	{
		$this->menus[4]['class'] = 'active';
		$data['menus'] = $this->menus;
		$data['userdata'] = $this->userdata;
        $data['view'] = 'panel/template';

        $data['content'] = 'panel/resepsionis/tambah_keluhan';

		$this->load->view('templates/admin', $data);
	}
}
