<?php

class User_model extends CI_Model
{
	private $table = "ms_user";

	function __construct()
	{
		parent::__construct();
	}

	function sign_in($username, $password)
	{
		$this->db->where('username', strtolower($username));
		$this->db->where('password', md5($password));
		
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
		{
			$last_login = date("Y-m-d H:i:s"); // gmt + 7
			$this->update_data($query->row()->id_user, array('last_login' => $last_login));
			
			return $query->row();
		}
		else
			return FALSE;
	}

	function register($username, $email, $password, $role, $nama)
	{
		$register_data = array(
			'username'		=> $username,
			'email'			=> $email,
			'password'		=> md5($password),
			'last_login'	=> date('Y-m-d H:i:s'),
			'create_date'	=> date('Y-m-d H:i:s'),
            'name'          => $nama,
            'id_role'          => $role
		);
		
		$this->db->insert($this->table, $register_data);

		return $this->db->insert_id();
	}
	
	function details($id_user)
	{
		$this->db->where('id_user', $id_user);
		$query = $this->db->get($this->table);

		return $query->row();
	}
    
    function listall()
    {
        $query = $this->db->get($this->table);
        
        return $query->result();
    }

	function update_data($id, $array)
	{
		$this->db->where('id_user', $id);
		$this->db->update($this->table, $array);
	}
	
	function delete($user_id)
	{
		$this->db->where('id_user', $user_id);
		$this->db->delete($this->table);
		
		return TRUE;
	}
	
	function check_username($username)
	{
		$username = strtolower($username);
		$this->db->where('username', $username);
		
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
			return FALSE;
		else 
			return TRUE;
	}
}