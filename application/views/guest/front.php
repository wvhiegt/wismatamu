<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/global.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/front.css" rel="stylesheet">
        <link href="assets/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="assets/images/logo/favicon.png" sizes="32x32">
        <script src="assets/js/jquery-2.1.3.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <title>TreesLife</title>
        <script>
        $( window ).scroll(function() {
            var scroll = $(window).scrollTop();
                // change class of navbar
            $navbar = $("nav.navbar");
            $hi = $("section").eq(1);

            // navbar
            if (scroll >= $hi.offset().top - $navbar.height() - 30)
            {
                $navbar.addClass('scrolled');
                $navbar.find('.img-logo').attr('src', 'assets/images/logo/treeslife circle.png');
            }
            else
            {
                $navbar.removeClass('scrolled');
                $navbar.find('.img-logo').attr('src', 'assets/images/logo/treeslife.png');
            }
        });
        </script>
        <script src="assets/js/global.js"></script>
        <script src="assets/js/front.js"></script>
        <script src="assets/vendors/nicescroll/jquery.nicescroll.min.js"> </script>
    </head>
    <body>
        <div class="wrapper">
            <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">

                <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img class="img-logo" src="assets/images/logo/treeslife.png" /> TreesLife</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="custom-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#intro">Home</a></li>
                            <li><a href="#today">Situation</a></li>
                            <li><a href="#actionT">Action</a></li>
                            <li><a href="#donate-login">Donate</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#register">Register</a></li>
                        </ul>
                    </div>

                </div>

            </nav>
            
            <section id="intro" class="section">

                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="hello wow bounceInDown">
                                <h4>Welcome to</h4>
                                <h1>TreesLife</h1>
                                <h3><span class="rotate">Green Action | Save the Tree | Save the World</span></h3>
                            </div>

                            <a href="#about">
                                <div class="mouse-icon">
                                    <div class="wheel"></div>
                                </div>
                            </a>

                        </div>

                    </div><!-- .row -->

                </div><!-- .container -->
                <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; z-index: -999998; position: absolute; width: 100%; height: 100%;">
                    <img src="assets/images/resource/1.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; max-height: none; max-width: none; z-index: -999999; width: 100%; height: 100%; left: 0; top: 0px;">
                </div>

            </section>
            
            <!-- Card -->
            <section id="today">
                <div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header" style="background-image: url(assets/images/resource/2.jpg);">
                                        <div class="card-header-mask">
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-body-header">
                                            <h1>In The Past: True Paradise on Earth</h1>
                                            <p class="card-body-header-sentence">
                                                They call it <span>"</span>God's Own Country.<span>"</span>
                                            </p>
                                        </div>
                                        <p class="card-body-description">
                                            Long time ago, there are still many trees in this world, the tree become the shelter and provide food to the                                             past humans. In other word, the tree is our                                                 mother of nature
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header" style="background-image: url(assets/images/resource/3.jpg);">
                                        <div class="card-header-mask">
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-body-header">
                                            <h1>In The Present: Road to Wasteland</h1>
                                            <p class="card-body-header-sentence">
                                                Humans almost destroyed everything
                                            </p>
                                        </div>
                                        <p class="card-body-description">
                                           The Earth is 4.6 billion years old. Scaling to 46 YEARS, we’ve been here 4 hours and our Industrial Revolution                                            began just 1 minute ago. In that time we’ve destroyed more than 50% of the world’s tree
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header" style="background-image: url(assets/images/resource/4.jpg);">
                                        <div class="card-header-mask">
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-body-header">
                                            <h1>In the Future: We Still Don't Know</h1>
                                            <p class="card-body-header-sentence">
                                                Nobody knows the future
                                            </p>
                                        </div>
                                        <p class="card-body-description">
                                            Now, the future is in your hands
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div><!-- .container -->
                </div>
            </section>
            
        </div>
        
        <!-- Threshold -->
        <section id="actionT">
            <div class="container" id="threshold">
                <div class="row">
                    <div class="col-md-8 col-xs-12 col-md-offset-2">
                        <div class="treshold-title">
                              <h1>The Treeshold</h1>        		
                            <p>As the total donation reaches each stretch goal, its action will be unlocked for everyone regardless of time of donate.
                            </p>
                        </div>                 
                    </div>
                </div>
                <?php
                foreach ($action as $row) : ?> 
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <div class="StretchGoalAmount Content">
                             $<?php echo $row->threshold ?>
                            <br>
                            <i class="fa fa-check-circle-o fa-3x" style="padding-top:10px;"></i>
                        <!--<br />Unlocked-->
                        </div>
                        <div class="StretchGoalsProgressBar Content" style="height: 200px;">
                            <div class="StretchGoalsProgressBar Content" style="height: <?php $x=($sum/$row->threshold)*100; if($x>100) $x=100; echo $x?>%; width: 100%; background-color: green">
                            </div>
                        </div>
                        <div class="StretchGoalReward" style="height: 200px; ">
                                    <div class="StretchGoalDesc">
                                        <div class="StretchGoalDescTitleText"><?php echo $row->action_name ?></div>
                                            <div class="StretchGoalDescText">
                                                <?php echo $row->action_desc ?>
                                            </div>
                                    </div>
                         </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </section>
        
        <!-- donation & Login -->
        <section id="donate-login">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-12 col-md-offset-2">
                        <div class="treshold-title">
                              <h1>Let's Rebuild the World Together</h1>		
                        </div>                 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="donasi">
                            <h2>Donate anonymously</h2>
                            <formx class="form-inline" action="index.php/pages/donate_anonymous" method="post">
                                <div class="form-group" style="margin-top: 5%;">
                                    <label class="sr-only" for="exampleInputAmount">Amount (in rupiah)</label>
                                    <div class="input-group">
                                          <div class="input-group-addon">Rp</div>
                                            <input type="text" class="form-control" name="InputAmount" placeholder="Amount">
                                          <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <button type="submit" class="btn btn-primary">Transfer cash</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="register-login">
                            <h2>Register Or Login</h2>
                            <div class="register-login-style">
                                <a href="#" class="btn btn-primary btn-block shadow" data-toggle="modal" data-target="#register">Register</a>
                                <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header" style="background-color: #5cb85c;">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" style="color: rgba(255, 255, 255, 0.87); text-align: center;" id="myModalLabel">Register Now</h3>
                                      </div>
                                      <div class="modal-body" style="background-color: #efefef;">
                                          <div class="login-test">
                                          <div class="login-content">
                                            <form action="index.php/pages/signup" method="post">
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-person"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="username" type="text" class="form-control" placeholder="Username" autofocus/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-sms"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="fullname" type="text" class="form-control" placeholder="Full Name" autofocus/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-email"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="email" type="email" class="form-control" placeholder="Email Address"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-lock"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="password" type="password" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                                <input type="submit" class="hidden" />
                                                <a href="javascript:;" class="btn btn-login btn-primary btn-float waves-effect waves-button submit-btn"><i class="md md-arrow-forward"></i></a>
                                            </form>
                                         </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <span id="button-or">or</span>
                                <a href="#" class="btn btn-success btn-block shadow" data-toggle="modal" data-target="#login">Login</a>
                                <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header" style="background-color: #5cb85c;">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title" style="color: rgba(255, 255, 255, 0.87); text-align: center;" id="myModalLabel">Login In Now</h3>
                                      </div>
                                      <div class="modal-body" style="background-color: #efefef;">
                                        <div class="login-test">
                                          <div class="login-content">
                                            <form action="index.php/pages/signin" method="post">
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-person"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="username" type="text" class="form-control" placeholder="Username" autofocus/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="fi-label fi-icon"><i class="md md-lock"></i></label>
                                                    <div class="fi-inline">
                                                        <input name="password" type="password" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                                <input type="submit" class="hidden" />
                                                <a href="javascript:;" class="btn btn-login btn-primary btn-float waves-effect waves-button submit-btn"><i class="md md-arrow-forward"></i></a>
                                            </form>
                                         </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
           
        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-4">
                            <h3>Location</h3>
                            <p>
                            KBS, Jl Suterejo Selatan VI no 2<br>
                            Surabaya, Jawa Timur, Indonesia 90210
                            </p>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Around the Web</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="//www.facebook.com/SaintGraveyard" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/+JulioAnthonyLeonards/posts" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/imjuanleonard" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>About Treeslife</h3>
                            <p>
                                Treeslife is a company that move in Green Action <br>
                                Our sole purpose is only for saving the world <br>
                                We can't close our eyes anymore at this deprecated world <br> 
                                So, Are you the one of us? <br>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; WP Dream Team<br/>
                            Part of KBS Project<br/>
                            2015
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        
        <!----js---->
        <script src="assets/js/jquery.backstretch.min.js"></script>

    </body>
</html>