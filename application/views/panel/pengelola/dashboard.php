
            <!-- BEGIN CONTENT-->
            <div id="content">
                <section>
                    <div class="section-body">
                        <div class="row">

                            <!-- BEGIN ALERT - REVENUE -->
                            <div class="col-md-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body no-padding">
                                        <div class="alert alert-callout alert-info no-margin">
                                            <strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>
                                            <strong class="text-xl">27.550.000,- </strong><br/>
                                            <span class="opacity-50">Pendapatan November 2015</span>
                                            <div class="stick-bottom-left-right">
                                                <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END ALERT - REVENUE -->

                            <!-- BEGIN ALERT - VISITS -->
                            <div class="col-md-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body no-padding">
                                        <div class="alert alert-callout alert-warning no-margin">
                                            <strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>
                                            <strong class="text-xl">102</strong><br/>
                                            <span class="opacity-50">Pengunjung</span>
                                            <div class="stick-bottom-right">
                                                <div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END ALERT - VISITS -->

                            <!-- BEGIN ALERT - BOUNCE RATES -->
                            <div class="col-md-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body no-padding">
                                        <div class="alert alert-callout alert-success no-margin">
                                            <strong class="pull-right text-danger text-lg">0,18% <i class="md md-trending-down"></i></strong>
                                            <strong class="text-xl">85.37%</strong><br/>
                                            <span class="opacity-50">Tingkat kepuasan pelanggan</span>
                                            <div class="stick-bottom-left-right">
                                                <div class="progress progress-hairline no-margin">
                                                    <div class="progress-bar progress-bar-success" style="width:83%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END ALERT - BOUNCE RATES -->

                            <!-- BEGIN ALERT - TIME ON SITE -->
                            <div class="col-md-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body no-padding">
                                        <div class="alert alert-callout alert-danger no-margin">
                                            <h1 class="pull-right text-danger"><i class="fa fa-archive"></i></h1>
                                            <strong class="text-xl">23</strong><br/>
                                            <span class="opacity-50">Fasilitas yang rusak/hilang</span>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END ALERT - TIME ON SITE -->

                        </div><!--end .row -->
                        <div class="row">

                            <!-- BEGIN SITE ACTIVITY -->
                            <div class="col-md-9">
                                <div class="card ">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card-head">
                                                <header>Site activity</header>
                                            </div><!--end .card-head -->
                                            <div class="card-body height-8">
                                                <div id="flot-visitors-legend" class="flot-legend-horizontal stick-top-right no-y-padding"></div>
                                                <div id="flot-visitors" class="flot height-7" data-title="Activity entry" data-color="#7dd8d2,#0aa89e"></div>
                                            </div><!--end .card-body -->
                                        </div><!--end .col -->
                                        <div class="col-md-4">
                                            <div class="card-head">
                                                <header>Today's stats</header>
                                            </div>
                                            <div class="card-body height-8">
                                                <strong>214</strong> members
                                                <span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-primary-dark" style="width:43%"></div>
                                                </div>
                                                756 pageviews
                                                <span class="pull-right text-success text-sm">0,68% <i class="md md-trending-up"></i></span>
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-primary-dark" style="width:11%"></div>
                                                </div>
                                                291 bounce rates
                                                <span class="pull-right text-danger text-sm">21,08% <i class="md md-trending-down"></i></span>
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-danger" style="width:93%"></div>
                                                </div>
                                                32,301 visits
                                                <span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-primary-dark" style="width:63%"></div>
                                                </div>
                                                132 pages
                                                <span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-primary-dark" style="width:47%"></div>
                                                </div>
                                            </div><!--end .card-body -->
                                        </div><!--end .col -->
                                    </div><!--end .row -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END SITE ACTIVITY -->

                            <!-- BEGIN SERVER STATUS -->
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-head">
                                        <header class="text-primary">Server status</header>
                                    </div><!--end .card-head -->
                                    <div class="card-body height-4">
                                        <div class="pull-right text-center">
                                            <em class="text-primary">Temperature</em>
                                            <br/>
                                            <div id="serverStatusKnob" class="knob knob-shadow knob-primary knob-body-track size-2">
                                                <input type="text" class="dial" data-min="0" data-max="100" data-thickness=".09" value="50" data-readOnly=true>
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                    <div class="card-body height-4 no-padding">
                                        <div class="stick-bottom-left-right">
                                            <div id="rickshawGraph" class="height-4" data-color1="#0aa89e" data-color2="rgba(79, 89, 89, 0.2)"></div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <!-- END SERVER STATUS -->

                        </div><!--end .row -->
                    </div><!--end .section-body -->
                </section>
            </div><!--end #content-->
            <!-- END CONTENT -->


            <!-- BEGIN OFFCANVAS RIGHT -->
            <div class="offcanvas">

                <!-- BEGIN OFFCANVAS SEARCH -->
                <div id="offcanvas-search" class="offcanvas-pane width-8">
                    <div class="offcanvas-head">
                        <header class="text-primary">Search</header>
                        <div class="offcanvas-tools">
                            <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                                <i class="md md-close"></i>
                            </a>
                        </div>
                    </div>
                    <div class="offcanvas-body no-padding">
                        <ul class="list ">
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>A</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar4.jpg?1404026791" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Alex Nelson
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar9.jpg?1404026744" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Ann Laurens
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>J</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar2.jpg?1404026449" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Jessica Cruise
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar8.jpg?1404026729" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Jim Peters
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>M</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar5.jpg?1404026513" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Mabel Logan
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar11.jpg?1404026774" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Mary Peterson
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar3.jpg?1404026799" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Mike Alba
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>N</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar6.jpg?1404026572" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Nathan Peterson
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>P</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar7.jpg?1404026721" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Philip Ericsson
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                            <li class="tile divider-full-bleed">
                                <div class="tile-content">
                                    <div class="tile-text"><strong>S</strong></div>
                                </div>
                            </li>
                            <li class="tile">
                                <a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                    <div class="tile-icon">
                                        <img src="assets/img/avatar10.jpg?1404026762" alt="" />
                                    </div>
                                    <div class="tile-text">
                                        Samuel Parsons
                                        <small>123-123-3210</small>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div><!--end .offcanvas-body -->
                </div><!--end .offcanvas-pane -->
                <!-- END OFFCANVAS SEARCH -->

            </div><!--end .offcanvas-->
            <!-- END OFFCANVAS RIGHT -->
