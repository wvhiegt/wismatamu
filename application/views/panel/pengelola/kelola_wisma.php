			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Pengelola</li>
							<li class="active">Kelola Wisma</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>Kelola Wisma</h4>
							</div><!--end .col -->
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="card card-collapsed">
									<div class="card-head card-head-sm style-primary">
										<div class="tools">
											<div class="btn-group">
												<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah Jenis Kamar" href='index.php/panel/pengelola/tambah_jenis_kamar' class="btn btn-icon-toggle"><i class="md md-add"></i></a>
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> List Jenis Kamar</header>
									</div>
									<div class="card-body" style="display:none">
										<ul class="list divider-full-bleed">
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Single Room - Rp. 200.000,00
														<small>
															<ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-hotel"></i> Single bed</li>
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-format-align-justify"></i> AC</li>  
								                            </ul>
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_jenis_kamar" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Double Room - Rp. 300.000,00
														<small>
															<ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-hotel"></i> Double bed</li>
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-tv"></i>TV Cable</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-format-align-justify"></i> AC</li>  
								                            </ul>
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_jenis_kamar" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Premium Room - Rp. 400.000,00
														<small>
															<ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-hotel"></i> King Double bed</li>
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-tv"></i>TV Cable</li>  
								                            </ul>
								                            <ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-restaurant-menu"></i> Breakfast</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-format-align-justify"></i> AC</li> 
								                            </ul>
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_jenis_kamar" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Wisma - Rp. 1.000.000,00
														<small>
															<ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-hotel"></i> Wisma</li>
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-tv"></i> TV Cable</li> 
								                            </ul>
								                            <ul style="list-style:none;float:left">
								                                <li style="display:inline-block;margin:5px 10px -15px -15px"><i class="md md-restaurant-menu"></i> Breakfast</li> 
								                                <li style="display:inline-block;margin:5px 10px -15px 10px"><i class="md md-format-align-justify"></i> AC</li> 
								                            </ul>
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_jenis_kamar" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div><!--end .col -->
							<div class="col-lg-6">
								<div class="card card-collapsed">
									<div class="card-head card-head-sm style-primary">
										<div class="tools">
											<div class="btn-group">
												<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah Fasilitas" href='index.php/panel/pengelola/tambah_fasilitas' class="btn btn-icon-toggle"><i class="md md-add"></i></a>
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> List Fasilitas</header>
									</div>
									<div class="card-body" style="display:none">
										<ul class="list divider-full-bleed">
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Tempat Tidur Tambahan (5)
														<small>
															Rp. 75.000,00
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_fasilitas" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Selimut (10)
														<small>
															Rp. 25.000,00
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_fasilitas" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
											<li class="tile">
												<a class="tile-content">
													<div class="tile-text">
														Bantal (10)
														<small>
															Rp. 25.000,00
														</small>
													</div>
												</a>
												<a href="index.php/panel/pengelola/update_fasilitas" class="btn btn-flat ink-reaction">
													<i class="md md-edit"></i>
												</a>
												<a class="btn btn-flat ink-reaction">
													<i class="md md-delete"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div><!--end .col -->
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-head card-head-sm style-primary">
										<div class="tools">
											<div class="btn-group">
												<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah Kamar" href='index.php/panel/pengelola/tambah_kamar' class="btn btn-icon-toggle"><i class="md md-add"></i></a>
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> List Kamar</header>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="datatable1" class="table table-striped table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th>Wisma</th>
														<th>No. Kamar</th>
														<th>Jenis Kamar</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Flamboyan</td>
														<td>F101</td>
														<td>Single Room</td>
														<td><a href="index.php/panel/pengelola/ubah_detail_kamar" class="btn btn-xs btn-warning"><i class="md md-edit"></i> Ubah</a></td>
													</tr>
													<tr>
														<td>2</td>
														<td>Flamboyan</td>
														<td>F102</td>
														<td>Single Room</td>
														<td><a href="index.php/panel/pengelola/ubah_detail_kamar" class="btn btn-xs btn-warning"><i class="md md-edit"></i> Ubah</a></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Flamboyan</td>
														<td>F103</td>
														<td>Double Room</td>
														<td><a href="index.php/panel/pengelola/ubah_detail_kamar" class="btn btn-xs btn-warning"><i class="md md-edit"></i> Ubah</a></td>
													</tr>
													<tr>
														<td>4</td>
														<td>Jasmine</td>
														<td>-</td>
														<td>Wisma</td>
														<td><a href="index.php/panel/pengelola/ubah_detail_kamar" class="btn btn-xs btn-warning"><i class="md md-edit"></i> Ubah</a></td>
													</tr>
												</tbody>
											</table>
										</div><!--end .table-responsive -->
									</div>
								</div>
							</div><!--end .col -->
						</div><!--end .row -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
$('#datatable2').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>