			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Pengelola</li>
							<li class="active">Rekapitulasi Keuangan</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>Rekapitulasi Keuangan</h4>
							</div><!--end .col -->
						</div>

						<div class="row">
							<div class="col-lg-12">
								<form class="form-inline">
									<div class="card">
										<div class="card-body">
											<div class="form-group floating-label">
												<div class="input-group date" id="demo-date-month">
													<div class="input-group-content">
														<input value="November 2015" placeholder="Bulan" type="text" class="form-control">
														<label style="margin-top:-20px">Bulan</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<button type="submit" class="btn btn-raised btn-default-light ink-reaction"><i class='md md-search'></i> Lihat Laporan</button>
										</div><!--end .card-body -->
									</div><!--end .card -->
								</form>
							</div><!--end .col -->

						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-body">
										<div class="alert alert-callout alert-info" style="padding:0;padding-left:15px">
											<h5>Pendapatan bulan November 2015 : <b>Rp. 27.550.000,00 </b></h5>
										</div>
										<div class="table-responsive">
											<table id="datatable1" class="table table-striped table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th>Tanggal Check Out</th>
														<th>No. Invoice</th>
														<th>Pemasukan</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>21 November 2015</td>
														<td><a href="#">12345678</a></td>
														<td>Rp. 605.000,00</td>
													</tr>
													<tr>
														<td>2</td>
														<td>18 November 2015</td>
														<td><a href="#">12345677</a></td>
														<td>Rp. 350.000,00</td>
													</tr>
													<tr>
														<td>3</td>
														<td>14 November 2015</td>
														<td><a href="#">12345676</a></td>
														<td>Rp. 700.000,00</td>
													</tr>
												</tbody>
											</table>
										</div><!--end .table-responsive -->
									</div>
								</div>
							</div><!--end .col -->
						</div><!--end .row -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
$('#demo-date-month').datepicker({autoclose: true, todayHighlight: true, minViewMode: 1, format:"MM yyyy"});

</script>