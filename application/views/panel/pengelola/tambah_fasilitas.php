			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Pengelola</li>
							<li>Kelola Wisma</li>
							<li class="active">Tambah Fasilitas</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
					<br/>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="card">
									<div class="card-head style-primary">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> Form Tambah Fasilitas</header>
									</div>
									<div class="card-body">
										<form class="form-horizontal" role="form">
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="Firstname5" class="col-sm-4 control-label">Nama Fasilitas</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="dollars17" class="col-sm-4 control-label">Harga</label>
														<div class="col-sm-8">
															<div class="input-group">
																<span class="input-group-addon">Rp. </span>
																<div class="input-group-content">
																	<input type="text" class="form-control" id="dollars17"><div class="form-control-line"></div>
																</div>
																<span class="input-group-addon">.00</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="Firstname5" class="col-sm-4 control-label">Kuantitas</label>
														<div class="col-sm-8">
															<input type="number" class="form-control" />
														</div>
													</div>
												</div>
											</div>
											
										</form>
									</div><!--end .card-body -->
									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<button type="submit" class="btn btn-flat btn-primary ink-reaction">Tambah</button>
										</div>
									</div>
								</div><!--end .card -->
							</div>
						</div>
						
					</div>
				</section>
			</div>