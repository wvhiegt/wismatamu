			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Pengelola</li>
							<li>Kelola Wisma</li>
							<li class="active">Tambah Kamar</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
					<br/>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="card">
									<div class="card-head style-primary">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> Form Tambah Kamar</header>
									</div>
									<div class="card-body">
										<form class="form-horizontal" role="form">
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="Firstname5" class="col-sm-4 control-label">Wisma</label>
														<div class="col-sm-8">
															<select id="select13" name="select13" class="form-control">
																<option>-- Wisma -- </option>
																<option>Bougenville</option>
																<option>Flamboyan</option>
																<option>Jasmine</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="Firstname5" class="col-sm-4 control-label">No. Kamar</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" placeholder="ex: F101" />
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="Firstname5" class="col-sm-4 control-label">Jenis Kamar</label>
														<div class="col-sm-8">
															<select id="select13" name="select13" class="form-control">
																<option>-- Jenis Kamar --</option>
																<option>Single Room</option>
																<option>Double Room</option>
																<option>Premium Room</option>
																<option>Wisma</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="textarea13" class="col-sm-4 control-label">Deskripsi</label>
														<div class="col-sm-8">
															<textarea name="textarea13" id="textarea13" class="form-control" rows="3" placeholder=""></textarea><div class="form-control-line"></div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div><!--end .card-body -->
									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<button type="submit" class="btn btn-flat btn-primary ink-reaction">Tambah</button>
										</div>
									</div>
								</div><!--end .card -->
							</div>
						</div>
						
					</div>
				</section>
			</div>