			<div id="content">
				<section class="style-default-bright">
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Petugas</li>
							<li>Periksa Inventaris Kamar</li>
							<li>List Kamar</li>
							<li class="active">Detail Laporan Inventaris Kamar</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>Lokasi Kamar : Wisma Flamboyan F105</h4>
								<h4>Tanggal Pemakaian : 19 November 2015 - 20 November 2015</h4>
							</div><!--end .col -->
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Fasilitas</th>
												<th>Status Fasilitas</th>
											</tr>
										</thead>
										<tbody>
											<tr class="gradeX">
												<th>1</th>
												<th>Meja</th>
												<th>Rusak</th>
											</tr>
											<tr class="gradeA">
												<th>2</th>
												<th>Pot Bunga</th>
												<th>Hilang</th>
											</tr>
										</tbody>
									</table>
								</div><!--end .table-responsive -->
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END DATATABLE 1 -->
					</div>
				</section>
			</div>