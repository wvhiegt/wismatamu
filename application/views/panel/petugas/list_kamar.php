			<div id="content">
				<section class="style-default-bright">
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Petugas</li>
							<li>Periksa Inventaris Kamar</li>
							<li class="active">List Kamar</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>List Kamar </h4>
							</div><!--end .col -->
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Wisma</th>
												<th>Kamar</th>
												<th>Status Pemeriksaan</th>
												<th>Tanggal Pemeriksaan</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr class="gradeX">
												<td>1</td>
												<td>Flamboyan</td>
												<td>F105</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox"></label></div></td>
												<td>-</td>
												<td><a href="index.php/panel/petugas/detail_inventaris_kamar" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/petugas/update_inventaris_kamar" class="btn btn-xs btn-warning"><i class="md md-add"></i> Laporan</a></td>
											</tr>
											<tr class="gradeC">
												<td>2</td>
												<td>Bougenville</td>
												<td>B101</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox" checked></label></div></td>
												<td>13/11/2015</td>
												<td><a href="index.php/panel/petugas/detail_inventaris_kamar" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/petugas/update_inventaris_kamar" class="btn btn-xs btn-warning"><i class="md md-add"></i> Laporan</a></td>
											</tr>
											<tr class="gradeA">
												<td>3</td>
												<td>Jasmine</td>
												<td>-</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox" checked></label></div></td>
												<td>10/11/2015</td>
												<td><a href="index.php/panel/petugas/detail_inventaris_kamar" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/petugas/update_inventaris_kamar" class="btn btn-xs btn-warning"><i class="md md-add"></i> Laporan</a></td>
											</tr>
										</tbody>
									</table>
								</div><!--end .table-responsive -->
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END DATATABLE 1 -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>