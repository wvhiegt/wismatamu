			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Petugas</li>
							<li>Periksa Inventaris Kamar</li>
							<li class="active">Form Pemeriksaan</li>
						</ol>
					</div>
					<div class="section-body contain-lg">

						<!-- BEGIN HORIZONTAL FORM - BASIC ELEMENTS-->
						<div class="card">
							<div class="card-head">
								<header><i class="fa fa-fw fa-tag"></i> Form Pemeriksaan</header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-2 control-label">Lokasi Kamar</label>
										<div class="col-sm-10">
											<p class="form-control-static">Wisma Flamboyan F105</p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Tanggal Pemakaian</label>
										<div class="col-sm-10">
											<p class="form-control-static">19 November 2015 - 20 November 2015</p>
										</div>
									</div>
									<div class="form-group">
										<label for="select13" class="col-sm-2 control-label">Fasilitas</label>
										<div class="col-sm-10">
											<select id="select13" name="select13" class="form-control">
												<option value="">&nbsp;</option>
												<option value="30">Meja</option>
												<option value="40">AC</option>
												<option value="50">Kursi</option>
												<option value="60">Pot Bunga</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="select13" class="col-sm-2 control-label">Status Fasilitas</label>
										<div class="col-sm-9">
											<label class="radio-inline radio-styled radio-danger">
												<input name="status" type="radio"><span>Hilang</span>
											</label>
											<label class="radio-inline radio-styled radio-warning">
												<input name="status" type="radio"><span>Rusak</span>
											</label>
										</div>
									</div>
									<div class="form-group">
										<label for="textarea13" class="col-sm-2 control-label">Deskripsi</label>
										<div class="col-sm-10">
											<textarea name="textarea13" id="textarea13" class="form-control" rows="3" placeholder=""></textarea>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" class="btn btn-flat btn-primary ink-reaction">Simpan</button>
								</div>
							</div>
						</div><!--end .card -->
						<!-- END HORIZONTAL FORM - BASIC ELEMENTS-->

					</div>
				</section>
			</div>