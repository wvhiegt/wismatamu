			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>Check In</li>
							<li class="active">Form Check In</li>
						</ol>
					</div>
					<div class="section-body contain-lg">

						<!-- BEGIN INTRO -->
						<div class="row">
							<div class="col-lg-12">
								<h1 class="text-primary">Check In </h1> 
							</div><!--end .col -->
							<div class="col-lg-8">
								<article class="margin-bottom-xxl">
									<p class="lead">
										
									</p>
								</article>
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END INTRO -->


						<!-- BEGIN HORIZONTAL FORM - BASIC ELEMENTS-->
						<div class="card">
							<div class="card-head style-primary">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<header><i class="fa fa-fw fa-tag"></i> Detail Reservasi</header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-2 control-label">Tanggal Menginap</label>
										<div class="col-sm-10">
											<p class="form-control-static">19 November 2015 - 21 November 2015</p>
										</div>
									</div>
									<div class="form-group">
											<label class="col-sm-2 control-label">Kamar Yang Dipesan</label>
										<div class="col-sm-10">
												<ul class="list divider-full-bleed">
													<li class="tile">
														<a class="tile-content ink-reaction">
															<div class="tile-text">
																Flamboyan F101 - Single Room
																<small>
																	<ul style="list-style:none;float:left;margin-bottom:-5px;margin-top:10px">
										                                <li style="display:inline-block;margin-left:-15px"><i class="md md-hotel"></i> Single bed</li>
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-format-align-justify"></i> AC</li>  
										                            </ul>
																</small>
															</div>
														</a>
													</li>
													<li class="tile">
														<a class="tile-content ink-reaction">
															<div class="tile-text">
																Flamboyan F102 - Single Room
																<small>
																	<ul style="list-style:none;float:left;margin-bottom:-5px;margin-top:10px">
										                                <li style="display:inline-block;margin-left:-15px"><i class="md md-hotel"></i> Single bed</li>
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-format-align-justify"></i> AC</li>  
										                            </ul>
																</small>
															</div>
														</a>
													</li>
												</ul>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Jumlah tamu</label>
										<div class="col-sm-10">
											<p class="form-control-static">2</p>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
						</div><!--end .card -->
						<!-- END Detail - BASIC ELEMENTS-->


						<!-- BEGIN HORIZONTAL FORM - BASIC ELEMENTS-->
						<div class="card">
							<div class="card-head style-primary">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<header><i class="fa fa-fw fa-tag"></i> Data Pemesan </header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label for="regular13" class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="regular13">
										</div>
									</div>
									<div class="form-group">
										<label for="placeholder13" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="placeholder13" placeholder="example@example.com">
										</div>
									</div>
									<div class="form-group">
										<label for="placeholder13" class="col-sm-2 control-label">No. Telepon</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="placeholder13" placeholder="+628 57 1234 5678">
										</div>
									</div>
									<div class="form-group">
										<label for="textarea13" class="col-sm-2 control-label">Alamat Pemesan</label>
										<div class="col-sm-10">
											<textarea name="textarea13" id="textarea13" class="form-control" rows="3" placeholder=""></textarea>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
						</div><!--end .card -->
						<!-- END HORIZONTAL FORM - BASIC ELEMENTS-->


						<div class="card">
							<div class="card-head style-primary">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<header><i class="fa fa-fw fa-tag"></i> Data Tamu </header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Firstname5" class="col-sm-4 control-label">Nama Tamu 1</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="Firstname5"><div class="form-control-line"></div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">ID(KTP,SIM,Passport)</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="Lastname5"><div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Firstname5" class="col-sm-4 control-label">Nama Tamu 2</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="Firstname5"><div class="form-control-line"></div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">ID(KTP,SIM,Passport)</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="Lastname5"><div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" class="btn btn-flat btn-primary ink-reaction">Check In</button>
								</div>
							</div>
						</div><!--end .card -->
					</div>
				</section>
			</div>