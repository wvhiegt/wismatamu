<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li class="active">Invoice</li>
						</ol>
					</div>
					<div class="section-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="card card-printable style-default-light">
									<div class="card-head">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
											</div>
										</div>
									</div><!--end .card-head -->
									<div class="card-body style-default-bright">

										<!-- BEGIN INVOICE HEADER -->
										<div class="row">
											<div class="col-xs-8">
												<h1 class="text-light"><i class="md md-hotel fa-fw fa-2x text-accent-dark"> </i> <strong class="text-accent-dark">Wisma Tamu</strong> ITS</h1>
											</div>
											<div class="col-xs-4 text-right">
												<h1 class="text-light text-default-light">Invoice</h1>
											</div>
										</div><!--end .row -->
										<!-- END INVOICE HEADER -->

										<br/>

										<!-- BEGIN INVOICE DESCRIPTION -->
										<div class="row">
											<div class="col-xs-4">
												<h4 class="text-light">Dibuat oleh</h4>
												<address>
													<strong>Wisma Tamu ITS</strong><br>
													Jl. Teknik Arsitektur Blok H 8 – 12, <br>
													Perumdos Kampus ITS, Keputih- Sukolilo, Surabaya<br>
													<abbr title="Phone">P:</abbr> (031) 5915 974
												</address>
											</div><!--end .col -->
											<div class="col-xs-4">
												<h4 class="text-light">Dibuat untuk</h4>
												<address>
													<strong>Bapak Nugroho Wicaksono</strong><br>
													<abbr title="Phone">P:</abbr> (+62) 857 1234 5678
												</address>
											</div><!--end .col -->
											<div class="col-xs-4">
												<div class="well">
													<div class="clearfix">
														<div class="pull-left"> INVOICE NO : </div>
														<div class="pull-right"> 98653624 </div>
													</div>
													<div class="clearfix">
														<div class="pull-left"> INVOICE DATE : </div>
														<div class="pull-right"> 19/11/2015 </div>
													</div>
												</div>
											</div><!--end .col -->
										</div><!--end .row -->
										<!-- END INVOICE DESCRIPTION -->

										<br/>

										<!-- BEGIN INVOICE PRODUCTS -->
										<div class="row">
											<div class="col-md-12">
												<table class="table">
													<thead>
														<tr>
															<th style="width:60px" class="text-center">Jumlah</th>
															<th class="text-left">Deskripsi</th>
															<th style="width:200px" class="text-right">Harga</th>
															<th style="width:150px" class="text-right">Total</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="text-center">1</td>
															<td>Kamar Single Room</td>
															<td class="text-right">Rp. 200.000,00</td>
															<td class="text-right">Rp. 200.000,00</td>
														</tr>
														<tr>
															<td class="text-center">1</td>
															<td>Kamar Double Room</td>
															<td class="text-right">Rp. 300.000,00</td>
															<td class="text-right">Rp. 300.000,00</td>
														</tr>
														<tr>
															<td class="text-center">2</td>
															<td>Selimut</td>
															<td class="text-right">Rp. 25.000,00</td>
															<td class="text-right">Rp. 50.000,00</td>
														</tr>
														<tr>
															<td colspan="2" rowspan="4">
																<h3 class="text-light opacity-50">Invoice notes</h3>
																<p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</small></p>
																<p><strong><em>Terima kasih atas kunjungannya.</em></strong></p>
															</td>
															<td class="text-right"><strong>Subtotal</strong></td>
															<td class="text-right">Rp. 550.000,00</td>
														</tr>
														<tr>
															<td class="text-right hidden-border"><strong>PPN (10%)</strong></td>
															<td class="text-right hidden-border">Rp. 55.000,00</td>
														</tr>
														<tr>
															<td class="text-right"><strong class="text-lg text-accent">Total</strong></td>
															<td class="text-right"><strong class="text-lg text-accent">Rp. 605.000,00</strong></td>
														</tr>
													</tbody>
												</table>
											</div><!--end .col -->
										</div><!--end .row -->
										<!-- END INVOICE PRODUCTS -->

									</div><!--end .card-body -->

									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<a href="index.php/panel/resepsionis/form_kepuasan_pelanggan" class="btn btn-flat btn-primary ink-reaction">Check Out</a>
										</div>
									</div>
								</div><!--end .card -->
							</div><!--end .col -->
						</div><!--end .row -->
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->