			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li class="active">Dashboard</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>Dashboard Resepsionis</h4>
							</div><!--end .col -->
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-head card-head-sm style-primary">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> List Reservasi</header>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="datatable1" class="table table-striped table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th>Email</th>
														<th>Wisma</th>
														<th>Jumlah Tamu</th>
														<th>Tanggal Reservasi</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<tr class="gradeX">
														<td>1</td>
														<td>-</td>
														<td>Flamboyan</td>
														<td>2</td>
														<td>14/11/2015</td>
														<td>Waiting for check in</td>
														<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
													</tr>
													<tr class="gradeC">
														<td>2</td>
														<td>example@example.com</td>
														<td>Bougenville</td>
														<td>1</td>
														<td>13/11/2015</td>
														<td>Waiting for check in</td>
														<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
													</tr>
													<tr class="gradeA">
														<td>3</td>
														<td>example2@example.com</td>
														<td>Jasmine</td>
														<td>1</td>
														<td>12/11/2015</td>
														<td>Expired</td>
														<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
													</tr>
												</tbody>
											</table>
										</div><!--end .table-responsive -->
									</div>
								</div>
							</div><!--end .col -->
						</div><!--end .row -->


						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-head card-head-sm style-primary">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> List Check In</header>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="datatable2" class="table table-striped table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th>Pemesan</th>
														<th>Kamar yang dipesan</th>
														<th>Jumlah Tamu</th>
														<th>Tanggal Berakhir</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Nugroho Wicaksono</td>
														<td>Flamboyan F101, Flamboyan F102</td>
														<td>2</td>
														<td>15/11/2015</td>
														<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
													</tr>
													<tr>
														<td>1</td>
														<td>Julio A. Leonard</td>
														<td>Bougenville B101, Bougenville B102</td>
														<td>2</td>
														<td>14/11/2015</td>
														<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
													</tr>
													<tr>
														<td>1</td>
														<td>Daniel Bintar</td>
														<td>Jasmine</td>
														<td>2</td>
														<td>13	/11/2015</td>
														<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
													</tr>
												</tbody>
											</table>
										</div><!--end .table-responsive -->
									</div>
								</div>
							</div><!--end .col -->
						</div><!--end .row -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
$('#datatable2').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>