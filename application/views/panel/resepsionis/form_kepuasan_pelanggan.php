<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li class="active">Form Kepuasan Pelanggan</li>
						</ol>
					</div>
					<div class="section-body">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<div class="card card-printable style-primary">
									<div class="card-head">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-tag"></i> Form Kepuasan Pelanggan</header>
									</div><!--end .card-head -->
									<div class="card-body style-default-bright">
										<form class="form-horizontal" role="form">
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-sm-4 control-label" style="text-align:left">Layanan Resepsionis</label>
														<div class="input-group">
															<div class="input-group-content form-control-static">
																<div id="slider1"></div>
															</div>
															<div class="input-group-addon" id="slider-value1">50</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-sm-4 control-label" style="text-align:left">Kebersihan Kamar</label>
														<div class="input-group">
															<div class="input-group-content form-control-static">
																<div id="slider2"></div>
															</div>
															<div class="input-group-addon" id="slider-value2">50</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-sm-4 control-label" style="text-align:left">Keramahan Petugas</label>
														<div class="input-group">
															<div class="input-group-content form-control-static">
																<div id="slider3"></div>
															</div>
															<div class="input-group-addon" id="slider-value3">50</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-sm-4 control-label" style="text-align:left">Kenyamanan Kamar</label>
														<div class="input-group">
															<div class="input-group-content form-control-static">
																<div id="slider4"></div>
															</div>
															<div class="input-group-addon" id="slider-value4">50</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<label class="col-sm-4 control-label" style="text-align:left">Kenyamanan Fasilitas</label>
														<div class="input-group">
															<div class="input-group-content form-control-static">
																<div id="slider5"></div>
															</div>
															<div class="input-group-addon" id="slider-value5">50</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div><!--end .card-body -->

									<div class="card-actionbar style-default-bright">
										<div class="card-actionbar-row">
											<a href="index.php/panel/resepsionis/form_kepuasan_pelanggan" class="btn btn-flat btn-success ink-reaction">Simpan</a>
										</div>
									</div>
								</div><!--end .card -->
							</div><!--end .col -->
						</div><!--end .row -->
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->
			<script>
				$("#slider1").slider({range: "min", value: 50, min: 0, max: 100,
					slide: function (event, ui) {
						$('#slider-value1').empty().append(ui.value);
					}
				})
				$("#slider2").slider({range: "min", value: 50, min: 0, max: 100,
					slide: function (event, ui) {
						$('#slider-value2').empty().append(ui.value);
					}
				})
				$("#slider3").slider({range: "min", value: 50, min: 0, max: 100,
					slide: function (event, ui) {
						$('#slider-value3').empty().append(ui.value);
					}
				})
				$("#slider4").slider({range: "min", value: 50, min: 0, max: 100,
					slide: function (event, ui) {
						$('#slider-value4').empty().append(ui.value);
					}
				})
				$("#slider5").slider({range: "min", value: 50, min: 0, max: 100,
					slide: function (event, ui) {
						$('#slider-value5').empty().append(ui.value);
					}
				})
			</script>