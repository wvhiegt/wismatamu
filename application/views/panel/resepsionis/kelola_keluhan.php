			<div id="content">
				<section class="style-default-bright">
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>Keluhan</li>
							<li class="active">List Keluhan</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>List Keluhan</h4>
							</div><!--end .col -->
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Lokasi</th>
												<th>Keluhan</th>
												<th>Waktu</th>
												<th>Terselesaikan ?</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>Flamboyan F101</td>
												<td>AC tidak bisa menyala</td>
												<td>15/11/2015 16:00</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox"></label></div></td>
											</tr>
											<tr>
												<td>2</td>
												<td>Flamboyan F103</td>
												<td>Keran air tidak berfungsi</td>
												<td>15/11/2015 14:00</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox"></label></div></td>
											</tr>
											<tr>
												<td>3</td>
												<td>Flamboyan F101</td>
												<td>TV tidak bisa menyala</td>
												<td>13/11/2015 16:00</td>
												<td><div class="checkbox checkbox-inline checkbox-styled"><label><input type="checkbox" checked></label></div></td>
											</tr>
										</tbody>
									</table>
								</div><!--end .table-responsive -->
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END DATATABLE 1 -->
						<div class="row">
							<div style="position:fixed;bottom:20px;right:20px">
								<a href="index.php/panel/resepsionis/tambah_keluhan" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-add"></i></a>
							</div>
						</div>
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>