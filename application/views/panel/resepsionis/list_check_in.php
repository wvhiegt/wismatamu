			<div id="content">
				<section class="style-default-bright">
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>Check Out</li>
							<li class="active">List Check In</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>List Check In </h4>
							</div><!--end .col -->
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Pemesan</th>
												<th>Kamar yang dipesan</th>
												<th>Jumlah Tamu</th>
												<th>Tanggal Berakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>Nugroho Wicaksono</td>
												<td>Flamboyan F101, Flamboyan F102</td>
												<td>2</td>
												<td>15/11/2015</td>
												<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
											</tr>
											<tr>
												<td>1</td>
												<td>Julio A. Leonard</td>
												<td>Bougenville B101, Bougenville B102</td>
												<td>2</td>
												<td>14/11/2015</td>
												<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
											</tr>
											<tr>
												<td>1</td>
												<td>Daniel Bintar</td>
												<td>Jasmine</td>
												<td>2</td>
												<td>13	/11/2015</td>
												<td><a href="index.php/panel/resepsionis/ubah_detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Ubah Detail</a> <a href="index.php/panel/resepsionis/check_out" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check Out</a></td>
											</tr>
										</tbody>
									</table>
								</div><!--end .table-responsive -->
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END DATATABLE 1 -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>