			<div id="content">
				<section class="style-default-bright">
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>Check In</li>
							<li class="active">List Reservasi</li>
						</ol>
					</div>
					<div class="section-body contain-lg">
						<div class="row">
							<div class="col-md-12">
								<h4>List Reservasi </h4>
							</div><!--end .col -->
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Email</th>
												<th>Wisma</th>
												<th>Jumlah Tamu</th>
												<th>Waktu Reservasi</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr class="gradeX">
												<td>1</td>
												<td>-</td>
												<td>Flamboyan</td>
												<td>2</td>
												<td>14/11/2015 22:03</td>
												<td>Waiting for check in</td>
												<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
											</tr>
											<tr class="gradeC">
												<td>2</td>
												<td>example@example.com</td>
												<td>Bougenville</td>
												<td>1</td>
												<td>13/11/2015 22:03</td>
												<td>Waiting for check in</td>
												<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
											</tr>
											<tr class="gradeA">
												<td>3</td>
												<td>example2@example.com</td>
												<td>Jasmine</td>
												<td>1</td>
												<td>12/11/2015 22:03</td>
												<td>Expired</td>
												<td><a href="index.php/panel/resepsionis/detail_reservasi" class="btn btn-xs btn-primary"><i class="md md-details"> </i> Detail</a> <a href="index.php/panel/resepsionis/check_in" class="btn btn-xs btn-warning"><i class="md md-add"></i> Check In</a></td>
											</tr>
										</tbody>
									</table>
								</div><!--end .table-responsive -->
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END DATATABLE 1 -->
					</div>
				</section>
			</div>
<script>
$('#datatable1').DataTable({
			"dom": 'lCfrtip',
			"order": [],
			"colVis": {
				"buttonText": "Columns",
				"overlayFade": 0,
				"align": "right"
			},
			"language": {
				"lengthMenu": '_MENU_ entries per page',
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>',
					"next": '<i class="fa fa-angle-right"></i>'
				}
			}
		});
</script>