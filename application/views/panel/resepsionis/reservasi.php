			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>Reservasi</li>
							<li class="active">Form Reservasi</li>
						</ol>
					</div>
					<div class="section-body contain-lg">

						<!-- BEGIN INTRO -->
						<div class="row">
							<div class="col-lg-12">
								<h1 class="text-primary">Reservasi </h1> 
							</div><!--end .col -->
							<div class="col-lg-8">
								<article class="margin-bottom-xxl">
									<p class="lead">
										
									</p>
								</article>
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END INTRO -->

						<div class="row">
							<div class="col-md-6">
								<div class="card">
									<div class="card-head style-accent">
										<header>Flamboyan F101 - Single Room</header>
									</div>
									<div class="card-body text-default-light">
										<p>
											Lorem ipsum dolor sit amet, pellentesque amet adipiscing scelerisque, vitae urna aenean, justo malesuada at eu, orci varius risus luctus enim a malesuada. Erat in tempor magna, eget porttitor posuere.
											<h3>Facility</h3>
											<ul style="list-style:none;float:left">
				                                <li style="display:inline-block;margin:5px 10px 5px -15px"><i class="md md-hotel"></i> Single bed</li>
				                                <li style="display:inline-block;margin:5px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
				                                <li style="display:inline-block;margin:5px 10px"><i class="md md-format-align-justify"></i> AC</li>  
				                            </ul>
										</p>
									</div><!--end .card-body -->
									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<div class="checkbox checkbox-styled">
												<label>
													<input type="checkbox" value="">
													<span>Pesan</span>
												</label>
											</div>
										</div>
									</div><!--end .card-actionbar -->
								</div><!--end .card -->
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-head style-accent">
										<header>Flamboyan F102 - Double Room</header>
									</div>
									<div class="card-body text-default-light">
										<p>
											Lorem ipsum dolor sit amet, pellentesque amet adipiscing scelerisque, vitae urna aenean, justo malesuada at eu, orci varius risus luctus enim a malesuada. Erat in tempor magna, eget porttitor posuere.
											<h3>Facility</h3>
											<ul style="list-style:none;float:left">
				                                <li style="display:inline-block;margin:5px 10px 5px -15px"><i class="md md-hotel"></i> Double bed</li>
				                                <li style="display:inline-block;margin:5px 10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
				                                <li style="display:inline-block;margin:5px 10px"><i class="md md-format-align-justify"></i> AC</li>  
				                            </ul>
										</p>
									</div><!--end .card-body -->
									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<div class="checkbox checkbox-styled">
												<label>
													<input type="checkbox" value="">
													<span>Pesan</span>
												</label>
											</div>
										</div>
									</div><!--end .card-actionbar -->
								</div><!--end .card -->
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-right">
								<a href="index.php/panel/resepsionis/reservasi" class="btn ink-reaction btn-floating-action btn-primary"><i class="fa fa-book"></i></a>
							</div>
						</div>
					</div>
				</section>
			</div>