			<div id="content">
				<section>
					<div class="section-header">
						<ol class="breadcrumb">
							<li>Resepsionis</li>
							<li>List Check In</li>
							<li class="active">Ubah detail reservasi</li>
						</ol>
					</div>
					<div class="section-body contain-lg">

						<!-- BEGIN INTRO -->
						<div class="row">
							<div class="col-lg-12">
								<h1 class="text-primary">Ubah Detail Reservasi</h1> 
							</div><!--end .col -->
							<div class="col-lg-8">
								<article class="margin-bottom-xxl">
									<p class="lead">
										
									</p>
								</article>
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END INTRO -->


						<!-- BEGIN HORIZONTAL FORM - BASIC ELEMENTS-->
						<div class="card">
							<div class="card-head style-primary">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<header><i class="fa fa-fw fa-tag"></i> Detail Reservasi</header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-2 control-label">Tanggal Menginap</label>
										<div class="col-sm-10">
											<div class="input-daterange input-group" id="demo-date-range">
												<div class="input-group-content">
													<input disabled type="text" class="form-control" name="start" value="11/19/2015" />
												</div>
												<span class="input-group-addon">to</span>
												<div class="input-group-content">
													<input type="text" class="form-control" name="end"  value="11/21/2015" />
													<div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
											<label class="col-sm-2 control-label">Kamar Yang Dipesan</label>
										<div class="col-sm-10">
												<ul class="list divider-full-bleed">
													<li class="tile">
														<a class="tile-content ink-reaction">
															<div class="tile-text">
																Flamboyan F101 - Single Room
																<small>
																	<ul style="list-style:none;float:left;margin-bottom:-5px;margin-top:10px">
										                                <li style="display:inline-block;margin-left:-15px"><i class="md md-hotel"></i> Single bed</li>
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-format-align-justify"></i> AC</li>  
										                            </ul>
																</small>
															</div>
														</a>
													</li>
													<li class="tile">
														<a class="tile-content ink-reaction">
															<div class="tile-text">
																Flamboyan F102 - Single Room
																<small>
																	<ul style="list-style:none;float:left;margin-bottom:-5px;margin-top:10px">
										                                <li style="display:inline-block;margin-left:-15px"><i class="md md-hotel"></i> Single bed</li>
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-signal-wifi-4-bar"></i> Wifi</li> 
										                                <li style="display:inline-block;margin-left:10px"><i class="md md-format-align-justify"></i> AC</li>  
										                            </ul>
																</small>
															</div>
														</a>
													</li>
												</ul>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Jumlah tamu</label>
										<div class="col-sm-10">
											<p class="form-control-static">2</p>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button id="ubah-reservasi" type="submit" class="btn btn-flat btn-primary ink-reaction">Ubah</button>
								</div>
							</div>
						</div><!--end .card -->
						<!-- END Detail - BASIC ELEMENTS-->

						<div class="card">
							<div class="card-head style-primary">
								<div class="tools">
									<div class="btn-group">
										<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
									</div>
								</div>
								<header><i class="fa fa-fw fa-tag"></i> Fasilitas Tambahan</header>
							</div>
							<div class="card-body">
								<form class="form-horizontal" role="form">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Firstname5" class="col-sm-4 control-label">Fasilitas</label>
												<div class="col-sm-8">
													<select disabled id="select13" name="select13" class="form-control">
														<option value="">&nbsp;</option>
														<option selected value="30">Tempat Tidur Tambahan</option>
														<option value="40">Selimut</option>
														<option value="50">Bantal</option>
														<option value="60">Handuk</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">Jumlah</label>
												<div class="col-sm-8">
													<input disabled="" value="1" type="number" class="form-control" id="Lastname5">
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">Total Harga</label>
												<div class="col-sm-8">
													<p class="form-control-static">Rp. 75.000,00</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Firstname5" class="col-sm-4 control-label">Fasilitas</label>
												<div class="col-sm-8">
													<select id="select13" name="select13" class="form-control">
														<option value="">&nbsp;</option>
														<option value="30">Tempat Tidur Tambahan</option>
														<option value="40">Selimut</option>
														<option value="50">Bantal</option>
														<option value="60">Handuk</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">Jumlah</label>
												<div class="col-sm-8">
													<input type="number" class="form-control" id="Lastname5">
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Lastname5" class="col-sm-4 control-label">Total Harga</label>
												<div class="col-sm-8">
													<p class="form-control-static">-</p>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div><!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" class="btn btn-flat btn-primary ink-reaction">Tambah</button>
								</div>
							</div>
						</div><!--end .card -->
					</div>
				</section>
			</div>
<script>
	$('#demo-date-range').datepicker({todayHighlight: true});

	$('#ubah-reservasi').on('click', function (e) {
		toastr.options.closeButton = false;
		toastr.options.progressBar = false;
		toastr.options.debug = false;
		toastr.options.positionClass = 'toast-bottom-right';
		toastr.options.showDuration = 333;
		toastr.options.hideDuration = 333;
		toastr.options.timeOut = 4000;
		toastr.options.extendedTimeOut = 4000;
		toastr.options.showEasing = 'swing';
		toastr.options.hideEasing = 'swing';
		toastr.options.showMethod = 'slideDown';
		toastr.options.hideMethod = 'slideUp';
		toastr.success('<i class="md md-done"></i> Berhasil ubah detail reservasi', '');
	});
</script>