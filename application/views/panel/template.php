
        <!-- BEGIN HEADER-->
        <header id="header" >
            <div class="headerbar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="headerbar-left">
                    <ul class="header-nav header-nav-options">
                        <li class="header-nav-brand" >
                            <div class="brand-holder">
                                <a href="html/dashboards/dashboard.html">
                                    <span class="text-lg text-bold text-primary">Sistem Informasi Wisma Tamu</span>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="headerbar-right">
                    <ul class="header-nav header-nav-options">
                        <li>
                            <!-- Search form -->
                            <form class="navbar-search" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
                                </div>
                                <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                            </form>
                        </li>
                        <?php if(strtolower($userdata['title']) == 'pengelola'): ?>
                        <li class="dropdown hidden-xs">
                            <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                                <i class="fa fa-bell"></i><sup class="badge style-danger">1</sup>
                            </a>
                            <ul class="dropdown-menu animation-expand">
                                <li class="dropdown-header">Notifikasi</li>
                                <li>
                                    <a class="alert alert-callout alert-error" href="javascript:void(0);">
                                        <i class="pull-right dropdown-avatar md md-insert-emoticon" style="font-size:35px"></i>
                                        <strong>Kepuasan Pelanggan</strong><br/>
                                        <small>Pelanggan merasa kurang puas</small>
                                    </a>
                                </li>
                                <li class="dropdown-header">Options</li>
                                <li><a href="html/pages/login.html">Lihat semua notifikasi <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                            </ul><!--end .dropdown-menu -->
                        </li><!--end .dropdown -->
                        <?php endif; ?>
                    </ul><!--end .header-nav-options -->
                    <ul class="header-nav header-nav-profile">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                <img src="assets/img/avatar1.jpg?1403934956" alt="" />
                                <span class="profile-info">
                                    <?php echo $userdata['name'] ?>
                                    <small><?php echo $userdata['title'] ?></small>
                                </span>
                            </a>
                            <ul class="dropdown-menu animation-dock">
                                <li class="dropdown-header">Config</li>
                                <li><a href="html/pages/profile.html">My profile</a></li>
                                <li class="divider"></li>
                                <li><a href="html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>
                                <li><a href="index.php/home/signin"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                            </ul><!--end .dropdown-menu -->
                        </li><!--end .dropdown -->
                    </ul><!--end .header-nav-profile -->
                </div><!--end #header-navbar-collapse -->
            </div>
        </header>
        <!-- END HEADER-->

        <!-- BEGIN BASE-->
        <div id="base" >

            <!-- BEGIN OFFCANVAS LEFT -->
            <div class="offcanvas">
            </div><!--end .offcanvas-->
            <!-- END OFFCANVAS LEFT -->

            <!-- BEGIN MENUBAR-->
            <div id="menubar" class="menubar-inverse ">
                <div class="menubar-fixed-panel">
                    <div>
                        <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="expanded">
                        <a href="html/dashboards/dashboard.html">
                            <span class="text-lg text-bold text-primary ">Sistem Informas Wisma Tamu</span>
                        </a>
                    </div>
                </div>
                <div class="menubar-scroll-panel">

                    <!-- BEGIN MAIN MENU -->
                    <ul id="main-menu" class="gui-controls">

                        <?php foreach($menus as $menu):?>
                        <!-- BEGIN Menu -->
                        <li>
                            <a href="<?php echo $menu['link'] ?>" class="<?php echo $menu['class'] ?>">
                                <div class="gui-icon"><i class="md md-<?php echo $menu['icon'] ?>"></i></div>
                                <span class="title"><?php echo $menu['name'] ?></span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END Menu -->   
                        <?php endforeach;?>

                    </ul><!--end .main-menu -->
                    <!-- END MAIN MENU -->

                    <div class="menubar-foot-panel">
                        <small class="no-linebreak hidden-folded">
                            <span class="opacity-75">Copyright &copy; 2015</span> <strong>APSI B - Team B</strong>
                        </small>
                    </div>
                </div><!--end .menubar-scroll-panel-->
            </div><!--end #menubar-->
            <!-- END MENUBAR -->

            <?php $this->load->view($content); ?>

        </div><!--end #base-->
        <!-- END BASE -->