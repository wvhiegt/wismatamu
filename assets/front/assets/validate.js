/* <![CDATA[ */


// Jquery validate form contact
jQuery(document).ready(function(){

	$check = 0;

	$('#submit-check-in').click(function(){
		$check = 1;
	});
	$('#submit-check').click(function(){
		$check = 2;
	});
	

	$('#check_avail').submit(function(){

		var action = $(this).attr('action');

		$("#message-booking").slideUp(750,function() {
		$('#message-booking').hide();

 		$('#submit-check')
			.after(' <i class="icon-spin3 animate-spin loader"></i>')
			.attr('disabled','disabled');

		$.post(action, {
			check_in: $('#check_in').val(),
			room_type: $('#room_type').val(),
			email: $('#email').val(),
			quantity: $('#quantity').val(),
			wisma: $('#wisma').val()
	
		},
			function(data){

				if ($check == 1) {
					if (data.match('success')) {
						document.getElementById('message-booking').innerHTML = '<div style="background:rgba(0,0,0,.7);color:green;font-weight:bold" class="error_message">Kamar Tersedia, silahkan pesan !</div>';
						$('#message-booking').slideDown('slow');
						$('#check_avail .loader').fadeOut('slow',function(){$(this).remove()});
						$('#submit-check').show();
						$('#submit-check').removeAttr('disabled');
						$("#submit-check-in").hide();
						$check = 1;
					} else  {
						document.getElementById('message-booking').innerHTML = data;
						$('#message-booking').slideDown('slow');
						$('#check_avail .loader').fadeOut('slow',function(){$(this).remove()});
						$('#submit-check').removeAttr('disabled');
					}
				} else if ($check == 2) {

					document.getElementById('message-booking').innerHTML = data;
					$('#message-booking').slideDown('slow');
					$('#check_avail .loader').fadeOut('slow',function(){$(this).remove()});
					$('#submit-check').removeAttr('disabled');
					if(data.match('success') != null) $('#check_avail').slideUp('slow');
				}
			}
		);

		});

		return false;

	});
		});
		

  /* ]]> */